# Cash Manager Server

## Usage

### Build:
```
./gradlew clean build
```

### Exec:
```
java -jar build/libs/cash-manager-server-0.0.1-SNAPSHOT.jar
```

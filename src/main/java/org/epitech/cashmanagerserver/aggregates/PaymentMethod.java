package org.epitech.cashmanagerserver.aggregates;

/**
 * The enum Payment method.
 */
public enum PaymentMethod {
    /**
     * Credit card payment method.
     */
    CREDIT_CARD,
    /**
     * Cheque payment method.
     */
    CHEQUE
}

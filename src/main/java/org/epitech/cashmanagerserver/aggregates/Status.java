package org.epitech.cashmanagerserver.aggregates;

/**
 * The enum Status.
 */
public enum Status {
    /**
     * Created status.
     */
    CREATED,
    /**
     * Executed status.
     */
    EXECUTED,
    /**
     * Succeed status.
     */
    SUCCEED,
    /**
     * Failed status.
     */
    FAILED,
    /**
     * Canceled status.
     */
    CANCELED
}

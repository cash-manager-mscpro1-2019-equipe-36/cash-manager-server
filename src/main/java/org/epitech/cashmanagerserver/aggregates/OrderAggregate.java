package org.epitech.cashmanagerserver.aggregates;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.epitech.cashmanagerserver.commands.*;
import org.epitech.cashmanagerserver.events.*;
import org.epitech.cashmanagerserver.models.OrderLineItemModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The type Order aggregate.
 */
@Aggregate
public class OrderAggregate {

    @AggregateIdentifier
    private String id;

    private String userName;

    private String status;

    private String currency;

    private String paymentMethod;

    private List<OrderLineItemModel> lineItems;

    private Number amount;

    private String paymentError;

    /**
     * Instantiates a new Order aggregate.
     */
    public OrderAggregate() {
    }

    /**
     * Instantiates a new Order aggregate.
     *
     * @param createOrderCommand the create order command
     */
    @CommandHandler
    public OrderAggregate(CreateOrderCommand createOrderCommand) {
        AggregateLifecycle.apply(new OrderCreatedEvent(
                createOrderCommand.id,
                createOrderCommand.command,
                createOrderCommand.status,
                createOrderCommand.userName)
        );
    }

    /**
     * Create events for CreateOrderCommand.
     *
     * @param orderCreatedEvent the order created event
     */
    @EventSourcingHandler
    protected void on(OrderCreatedEvent orderCreatedEvent) {
        this.id = orderCreatedEvent.id;
        this.userName = orderCreatedEvent.userName;
        this.status = String.valueOf(Status.CREATED);
        this.lineItems = new ArrayList<>();
    }

    /**
     * Command that allow AddLineItem.
     *
     * @param addLineItemCommand the add line item command
     */
    @CommandHandler
    protected void on(AddLineItemCommand addLineItemCommand) {
        AggregateLifecycle.apply(new LineItemAddedEvent(
                addLineItemCommand.id,
                addLineItemCommand.command,
                addLineItemCommand.productId,
                addLineItemCommand.name,
                addLineItemCommand.price,
                addLineItemCommand.currency,
                addLineItemCommand.quantity
        ));
    }

    /**
     * Create events for AddLineItemCommand.
     *
     * @param lineItemAddedEvent the line item added event
     */
    @EventSourcingHandler
    protected void on(LineItemAddedEvent lineItemAddedEvent) {
        this.lineItems.add(new OrderLineItemModel(
                lineItemAddedEvent.productId,
                lineItemAddedEvent.name,
                lineItemAddedEvent.price,
                lineItemAddedEvent.currency,
                lineItemAddedEvent.quantity
        ));
    }

    /**
     * Command that allow RemoveLineItem.
     *
     * @param removeLineItemCommand the remove line item command
     */
    @CommandHandler
    protected void on(RemoveLineItemCommand removeLineItemCommand) {
        AggregateLifecycle.apply(new LineItemRemovedEvent(
                removeLineItemCommand.id,
                removeLineItemCommand.command,
                removeLineItemCommand.productId,
                removeLineItemCommand.name,
                removeLineItemCommand.price,
                removeLineItemCommand.currency,
                removeLineItemCommand.quantity
        ));
    }

    /**
     * Create events for RemoveLineItemCommand.
     *
     * @param lineItemRemovedEvent the line item removed event
     */
    @EventSourcingHandler
    protected void on(LineItemRemovedEvent lineItemRemovedEvent) {
        Iterator<OrderLineItemModel> itr = this.lineItems.iterator();

        while (itr.hasNext()) {
            if (itr.next().id.equals(lineItemRemovedEvent.productId))
                itr.remove();
        }
    }

    /**
     * Command that allow RemoveLineItem.
     *
     * @param setPaymentMethodCommand the set payment method command
     */
    @CommandHandler
    protected void on(setPaymentMethodCommand setPaymentMethodCommand) {
        AggregateLifecycle.apply(new PaymentMethodSetEvent(
                setPaymentMethodCommand.id,
                setPaymentMethodCommand.command,
                setPaymentMethodCommand.currency,
                setPaymentMethodCommand.paymentMethod.toString()
        ));
    }

    /**
     * Create events for setPaymentMethodCommand.
     *
     * @param paymentMethodSetEvent the payment method set event
     */
    @EventSourcingHandler
    protected void on(PaymentMethodSetEvent paymentMethodSetEvent) {
        this.currency = paymentMethodSetEvent.currency;
        this.paymentMethod = paymentMethodSetEvent.paymentMethod;
    }

    /**
     * Command that allow ExecuteOrder.
     *
     * @param executeOrderCommand the execute order command
     */
    @CommandHandler
    protected void on(ExecuteOrderCommand executeOrderCommand) {
        AggregateLifecycle.apply(new OrderExecutedEvent(
                executeOrderCommand.id,
                executeOrderCommand.command
        ));
    }

    /**
     * Create events for ExecuteOrderCommand.
     *
     * @param orderExecutedEvent the order executed event
     */
    @EventSourcingHandler
    protected void on(OrderExecutedEvent orderExecutedEvent) {
        Iterator<OrderLineItemModel> lineItem = this.lineItems.iterator();
        double total = 0;

        while (lineItem.hasNext())
            total += lineItem.next().price.doubleValue();
        this.status = String.valueOf(Status.EXECUTED);

        // TODO : Implement banking server call for payment

        if (total > 0) {
            AggregateLifecycle.apply(new OrderPaymentSucceedEvent(
                    orderExecutedEvent.id,
                    orderExecutedEvent.command,
                    "OrderPaymentSucceed",
                    total,
                    this.paymentMethod,
                    this.currency
            ));
        } else {
            AggregateLifecycle.apply(new OrderPaymentFailedEvent(
                    orderExecutedEvent.id,
                    orderExecutedEvent.command,
                    "OrderPaymentFailed",
                    this.paymentMethod,
                    this.currency,
                    "reason"
            ));
        }

    }

    /**
     * Create events for OrderExecutedEvent.
     *
     * @param orderPaymentSucceedEvent the order payment succeed event
     */
    @EventSourcingHandler
    protected void on(OrderPaymentSucceedEvent orderPaymentSucceedEvent) {
        this.status = String.valueOf(Status.SUCCEED);
        this.amount = orderPaymentSucceedEvent.amount;
    }

    /**
     * Create events for OrderExecutedEvent.
     *
     * @param orderPaymentFailedEvent the order payment failed event
     */
    @EventSourcingHandler
    protected void on(OrderPaymentFailedEvent orderPaymentFailedEvent) {
        this.status = String.valueOf(Status.FAILED);
        this.paymentError = orderPaymentFailedEvent.reason;
    }

}

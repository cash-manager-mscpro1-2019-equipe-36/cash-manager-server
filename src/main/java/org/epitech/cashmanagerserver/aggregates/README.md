# About aggregates

An Aggregate is an entity or group of entities that is always kept in a consistent state (within a single ACID transaction). 
The Aggregate Root is the entity within the aggregate that is responsible for maintaining this consistent state. 
This makes the aggregate the prime building block for implementing a command model in any CQRS based application.

## Axon framework

- ```@Aggregate``` annotation tells Axon that this entity will be managed by Axon. Basically, this is similar to ```@Entity``` annotation available with JPA. However, we will be using the Axon recommended annotation.
- ```@AggregateIdentifier``` annotation is used for the identifying a particular instance of the Aggregate. In other words, this is similar to JPA’s ```@Id``` annotation.

## Command handling

We are handling commands in their own handler methods. 
Each command must have an handler method.
These handler methods should be annotated with ```@CommandHandler``` annotation.
The handler methods use AggregateLifecyle.apply() method to register events.

## Event handling

These events are handled by methods annotated with ```@EventSourcingHandler``` annotation.
Also, it is imperative that __all state changes in an event sourced aggregate should be performed in these methods__.
Another important point to keep in mind is that the Aggregate Identifier must be set in the first method annotated with @EventSourcingHandler. 
In other words, this will be the creation Event.

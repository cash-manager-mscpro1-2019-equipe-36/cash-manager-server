package org.epitech.cashmanagerserver.controllers.queries;

import io.swagger.annotations.Api;
import org.epitech.cashmanagerserver.services.queries.OrderQueryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Order query controller.
 */
@RestController
@RequestMapping(value = "/orders")
@Api(value = "Orders Queries", tags = "Orders Queries")
public class OrderQueryController {

    private final OrderQueryService orderQueryService;

    /**
     * Instantiates a new Order query controller.
     *
     * @param orderQueryService the order query service
     */
    public OrderQueryController(OrderQueryService orderQueryService) {
        this.orderQueryService = orderQueryService;
    }

    /**
     * List events for transaction list.
     *
     * @param orderId the order id
     * @return the list
     */
    @GetMapping("/{orderId}/events")
    public List<Object> listEventsForTransaction(@PathVariable(value = "orderId") String orderId) {
        return this.orderQueryService.listEventsForOrder(orderId);
    }

}

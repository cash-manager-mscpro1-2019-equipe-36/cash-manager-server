package org.epitech.cashmanagerserver.controllers;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Server controller.
 */
@RestController
@RequestMapping(value = "/server")
@Api(value = "Server", tags = "Server")
public class ServerController {

    /**
     * Gets server health.
     *
     * @return the server health
     */
    @GetMapping("/healthcheck")
    public String getServerHealth() {
        return "OK";
    }

}

package org.epitech.cashmanagerserver.controllers.commands;

import io.swagger.annotations.Api;
import org.epitech.cashmanagerserver.dto.commands.OrderCreateDTO;
import org.epitech.cashmanagerserver.dto.commands.OrderLineItemDTO;
import org.epitech.cashmanagerserver.dto.commands.PaymentMethodDTO;
import org.epitech.cashmanagerserver.services.commands.OrderCommandService;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

/**
 * The type Order command controller.
 */
@RestController
@RequestMapping(value = "/orders")
@Api(value = "Orders Commands", tags = "Orders Commands")
public class OrderCommandController {

    private final OrderCommandService orderCommandService;

    /**
     * Instantiates a new Order command controller.
     *
     * @param orderCommandService the order command service
     */
    public OrderCommandController(OrderCommandService orderCommandService) {
        this.orderCommandService = orderCommandService;
    }

    /**
     * Create order completable future.
     *
     * @param orderCreateDTO the order create dto
     * @return the completable future
     */
    @PostMapping(value = "/create")
    public CompletableFuture<String> createOrder(@RequestBody OrderCreateDTO orderCreateDTO) {
        return this.orderCommandService.createOrder(orderCreateDTO);
    }

    /**
     * Add line item completable future.
     *
     * @param orderNumber      the order number
     * @param orderLineItemDTO the order line item dto
     * @return the completable future
     */
    @PutMapping(value = "/{orderNumber}/add_line_item")
    public CompletableFuture<String> addLineItem(@PathVariable String orderNumber,
                                                 @RequestBody OrderLineItemDTO orderLineItemDTO) {
        return this.orderCommandService.addLineItem(orderNumber, orderLineItemDTO);
    }

    /**
     * Remove line item completable future.
     *
     * @param orderNumber      the order number
     * @param orderLineItemDTO the order line item dto
     * @return the completable future
     */
    @PutMapping(value = "/{orderNumber}/remove_line_item")
    public CompletableFuture<String> removeLineItem(@PathVariable String orderNumber,
                                                    @RequestBody OrderLineItemDTO orderLineItemDTO) {
        return this.orderCommandService.removeLineItem(orderNumber, orderLineItemDTO);
    }

    /**
     * Sets payment method.
     *
     * @param orderNumber      the order number
     * @param paymentMethodDTO the payment method dto
     * @return the payment method
     */
    @PutMapping(value = "/{orderNumber}/set_payment_method")
    public CompletableFuture<String> setPaymentMethod(@PathVariable String orderNumber,
                                                      @RequestBody PaymentMethodDTO paymentMethodDTO) {
        return this.orderCommandService.setPaymentMethod(orderNumber, paymentMethodDTO);
    }

    /**
     * Execute order completable future.
     *
     * @param orderNumber the order number
     * @return the completable future
     */
    @PutMapping(value = "/{orderNumber}/execute")
    public CompletableFuture<String> executeOrder(@PathVariable String orderNumber) {
        return this.orderCommandService.executeOrder(orderNumber);
    }

}

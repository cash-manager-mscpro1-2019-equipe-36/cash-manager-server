package org.epitech.cashmanagerserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * The type Swagger config.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Api docket docket.
     *
     * @return the docket
     */
    @Bean
    public Docket apiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.epitech.cashmanagerserver"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo(){
        return new ApiInfo(
                "Cash Manager Server",
                "Java back-end service for Cash Manager project using Event Sourcing and CQRS with Axon and Spring Boot frameworks.",
                "0.0.1",
                "Terms of Service",
                new Contact("Epitech", "epitech.eu", ""),
                "MIT",
                "https://fr.wikipedia.org/wiki/Licence_MIT",
                Collections.emptyList());
    }

}

package org.epitech.cashmanagerserver.services.queries;

import java.util.List;

/**
 * The interface Order query service.
 */
public interface OrderQueryService {

    /**
     * List events for order list.
     *
     * @param transactionNumber the transaction number
     * @return the list
     */
    public List<Object> listEventsForOrder(String transactionNumber);

}

package org.epitech.cashmanagerserver.services.queries;

import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Order query service.
 */
@Service
public class OrderQueryServiceImpl implements OrderQueryService {

    /**
     * The Event store.
     */
    public final EventStore eventStore;

    /**
     * Instantiates a new Order query service.
     *
     * @param eventStore the event store
     */
    public OrderQueryServiceImpl(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Override
    public List<Object> listEventsForOrder(String orderId) {
        return eventStore.readEvents(orderId)
                .asStream()
                .map(s -> s.getPayload()).collect(Collectors.toList());
    }

}

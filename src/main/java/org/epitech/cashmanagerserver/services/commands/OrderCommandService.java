package org.epitech.cashmanagerserver.services.commands;

import org.epitech.cashmanagerserver.dto.commands.OrderCreateDTO;
import org.epitech.cashmanagerserver.dto.commands.OrderLineItemDTO;
import org.epitech.cashmanagerserver.dto.commands.PaymentMethodDTO;

import java.util.concurrent.CompletableFuture;

/**
 * The interface Order command service.
 */
public interface OrderCommandService {

    /**
     * Create order completable future.
     *
     * @param orderCreateDTO the order create dto
     * @return the completable future
     */
    CompletableFuture<String> createOrder(OrderCreateDTO orderCreateDTO);

    /**
     * Add line item completable future.
     *
     * @param orderNumber      the order number
     * @param orderLineItemDTO the order line item dto
     * @return the completable future
     */
    CompletableFuture<String> addLineItem(String orderNumber, OrderLineItemDTO orderLineItemDTO);

    /**
     * Remove line item completable future.
     *
     * @param orderNumber      the order number
     * @param orderLineItemDTO the order line item dto
     * @return the completable future
     */
    CompletableFuture<String> removeLineItem(String orderNumber, OrderLineItemDTO orderLineItemDTO);

    /**
     * Sets payment method.
     *
     * @param orderNumber      the order number
     * @param paymentMethodDTO the payment method dto
     * @return the payment method
     */
    CompletableFuture<String> setPaymentMethod(String orderNumber, PaymentMethodDTO paymentMethodDTO);

    /**
     * Execute order completable future.
     *
     * @param orderNumber the order number
     * @return the completable future
     */
    CompletableFuture<String> executeOrder(String orderNumber);

}

package org.epitech.cashmanagerserver.services.commands;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.epitech.cashmanagerserver.aggregates.Status;
import org.epitech.cashmanagerserver.commands.*;
import org.epitech.cashmanagerserver.dto.commands.OrderCreateDTO;
import org.epitech.cashmanagerserver.dto.commands.OrderLineItemDTO;
import org.epitech.cashmanagerserver.dto.commands.PaymentMethodDTO;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * The type Order command service.
 */
@Service
public class OrderCommandServiceImpl implements OrderCommandService {

    private final CommandGateway commandGateway;

    /**
     * Instantiates a new Order command service.
     *
     * @param commandGateway the command gateway
     */
    public OrderCommandServiceImpl(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public CompletableFuture<String> createOrder(OrderCreateDTO orderCreateDTO) {
        return this.commandGateway.send(new CreateOrderCommand(
                UUID.randomUUID().toString(),
                "createOrder",
                Status.CREATED,
                orderCreateDTO.getUserName())
        );
    }

    @Override
    public CompletableFuture<String> addLineItem(String orderNumber, OrderLineItemDTO orderLineItemDTO) {
        return this.commandGateway.send(new AddLineItemCommand(
                orderNumber,
                "addLineItem",
                orderLineItemDTO.getId(),
                orderLineItemDTO.getName(),
                orderLineItemDTO.getPrice(),
                orderLineItemDTO.getCurrency(),
                orderLineItemDTO.getQuantity()
        ));
    }

    @Override
    public CompletableFuture<String> removeLineItem(String orderNumber, OrderLineItemDTO orderLineItemDTO) {
        return this.commandGateway.send(new RemoveLineItemCommand(
                orderNumber,
                "removeLineItem",
                orderLineItemDTO.getId(),
                orderLineItemDTO.getName(),
                orderLineItemDTO.getPrice(),
                orderLineItemDTO.getCurrency(),
                orderLineItemDTO.getQuantity()
        ));
    }

    @Override
    public CompletableFuture<String> setPaymentMethod(String orderNumber, PaymentMethodDTO paymentMethodDTO) {
        return this.commandGateway.send(new setPaymentMethodCommand(
                orderNumber,
                "setPaymentMethod",
                paymentMethodDTO.getCurrency(),
                paymentMethodDTO.getPaymentMethod()
        ));
    }

    @Override
    public CompletableFuture<String> executeOrder(String orderNumber) {
        return this.commandGateway.send(new ExecuteOrderCommand(
                orderNumber,
                "executeOrder"
        ));
    }
}

package org.epitech.cashmanagerserver.events;

/**
 * The type Order executed event.
 */
public class OrderExecutedEvent extends BaseEvent<String> {

    /**
     * Instantiates a new Order executed event.
     *
     * @param id      the id
     * @param command the command
     */
    public OrderExecutedEvent(String id, String command) {
        super(id, command);
    }

}

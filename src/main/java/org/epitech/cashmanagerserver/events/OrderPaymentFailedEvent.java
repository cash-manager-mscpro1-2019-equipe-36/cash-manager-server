package org.epitech.cashmanagerserver.events;

/**
 * The type Order payment failed event.
 */
public class OrderPaymentFailedEvent extends BaseEvent<String> {

    /**
     * The Order payment status.
     */
    public final String orderPaymentStatus;

    /**
     * The Payment method.
     */
    public final String paymentMethod;

    /**
     * The Currency.
     */
    public final String currency;

    /**
     * The Reason.
     */
    public final String reason;

    /**
     * Instantiates a new Order payment failed event.
     *
     * @param id                 the id
     * @param command            the command
     * @param orderPaymentStatus the order payment status
     * @param paymentMethod      the payment method
     * @param currency           the currency
     * @param reason             the reason
     */
    public OrderPaymentFailedEvent(String id, String command, String orderPaymentStatus, String paymentMethod, String currency, String reason) {
        super(id, command);
        this.orderPaymentStatus = orderPaymentStatus;
        this.paymentMethod = paymentMethod;
        this.currency = currency;
        this.reason = reason;
    }

}

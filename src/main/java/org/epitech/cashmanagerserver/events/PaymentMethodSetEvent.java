package org.epitech.cashmanagerserver.events;

/**
 * The type Payment method set event.
 */
public class PaymentMethodSetEvent extends BaseEvent<String> {

    /**
     * The Currency.
     */
    public final String currency;

    /**
     * The Payment method.
     */
    public final String paymentMethod;

    /**
     * Instantiates a new Payment method set event.
     *
     * @param id            the id
     * @param command       the command
     * @param currency      the currency
     * @param paymentMethod the payment method
     */
    public PaymentMethodSetEvent(String id, String command, String currency, String paymentMethod) {
        super(id, command);
        this.currency = currency;
        this.paymentMethod = paymentMethod;
    }

}

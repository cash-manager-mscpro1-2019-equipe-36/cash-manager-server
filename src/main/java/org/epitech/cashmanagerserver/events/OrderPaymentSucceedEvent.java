package org.epitech.cashmanagerserver.events;

/**
 * The type Order payment succeed event.
 */
public class OrderPaymentSucceedEvent extends BaseEvent<String> {

    /**
     * The Order payment status.
     */
    public final String orderPaymentStatus;

    /**
     * The Amount.
     */
    public final Number amount;

    /**
     * The Payment method.
     */
    public final String paymentMethod;

    /**
     * The Currency.
     */
    public final String currency;

    /**
     * Instantiates a new Order payment succeed event.
     *
     * @param id                 the id
     * @param command            the command
     * @param orderPaymentStatus the order payment status
     * @param amount             the amount
     * @param paymentMethod      the payment method
     * @param currency           the currency
     */
    public OrderPaymentSucceedEvent(String id, String command, String orderPaymentStatus, Number amount, String paymentMethod, String currency) {
        super(id, command);
        this.orderPaymentStatus = orderPaymentStatus;
        this.amount = amount;
        this.paymentMethod = paymentMethod;
        this.currency = currency;
    }

}

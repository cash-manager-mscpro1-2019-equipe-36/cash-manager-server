package org.epitech.cashmanagerserver.events;

import org.epitech.cashmanagerserver.aggregates.Status;

/**
 * The type Order created event.
 */
public class OrderCreatedEvent extends BaseEvent<String> {

    /**
     * The Status.
     */
    public final Status status;

    /**
     * The User name.
     */
    public final String userName;

    /**
     * Instantiates a new Order created event.
     *
     * @param id       the id
     * @param command  the command
     * @param status   the status
     * @param userName the user name
     */
    public OrderCreatedEvent(String id, String command, Status status, String userName) {
        super(id, command);
        this.status = status;
        this.userName = userName;
    }

}

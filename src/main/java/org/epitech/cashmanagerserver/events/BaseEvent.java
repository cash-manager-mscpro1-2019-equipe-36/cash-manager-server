package org.epitech.cashmanagerserver.events;

/**
 * The type Base event.
 *
 * @param <T> the type parameter
 */
public class BaseEvent<T> {

    /**
     * The Id.
     */
    public final T id;
    /**
     * The Command.
     */
    public final T command;

    /**
     * Instantiates a new Base event.
     *
     * @param id      the id
     * @param command the command
     */
    public BaseEvent(T id, T command) {
        this.command = command;
        this.id = id;
    }

}

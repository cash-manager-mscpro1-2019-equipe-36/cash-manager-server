package org.epitech.cashmanagerserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Cash manager server application.
 */
@SpringBootApplication
public class CashManagerServerApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(CashManagerServerApplication.class, args);
	}

}

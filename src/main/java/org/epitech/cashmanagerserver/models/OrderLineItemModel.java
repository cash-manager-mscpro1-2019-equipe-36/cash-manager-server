package org.epitech.cashmanagerserver.models;

/**
 * The type Order line item model.
 */
public class OrderLineItemModel {

    /**
     * The Id.
     */
    public final String id;

    /**
     * The Name.
     */
    public final String name;

    /**
     * The Price.
     */
    public final Number price;

    /**
     * The Currency.
     */
    public final String currency;

    /**
     * The Quantity.
     */
    public final Number quantity;

    /**
     * Instantiates a new Order line item model.
     *
     * @param id       the id
     * @param name     the name
     * @param price    the price
     * @param currency the currency
     * @param quantity the quantity
     */
    public OrderLineItemModel(String id, String name, Number price, String currency, Number quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.currency = currency;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrderLineItemModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", currency='" + currency + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}

package org.epitech.cashmanagerserver.dto.commands;

import org.epitech.cashmanagerserver.aggregates.PaymentMethod;

/**
 * The type Payment method dto.
 */
public class PaymentMethodDTO {

    private String currency;

    private PaymentMethod paymentMethod;

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Gets payment method.
     *
     * @return the payment method
     */
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets payment method.
     *
     * @param paymentMethod the payment method
     */
    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

}

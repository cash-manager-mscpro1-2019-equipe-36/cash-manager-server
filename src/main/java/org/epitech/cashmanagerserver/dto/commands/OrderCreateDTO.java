package org.epitech.cashmanagerserver.dto.commands;

/**
 * The type Order create dto.
 */
public class OrderCreateDTO {

    private String userName;

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

}

package org.epitech.cashmanagerserver.commands;

/**
 * The type Execute order command.
 */
public class ExecuteOrderCommand extends BaseCommand<String> {

    /**
     * Instantiates a new Execute order command.
     *
     * @param id      the id
     * @param command the command
     */
    public ExecuteOrderCommand(String id, String command) {
        super(id, command);
    }

}

package org.epitech.cashmanagerserver.commands;

import org.epitech.cashmanagerserver.aggregates.PaymentMethod;

/**
 * The type Set payment method command.
 */
public class setPaymentMethodCommand extends BaseCommand<String> {

    /**
     * The Currency.
     */
    public final String currency;

    /**
     * The Payment method.
     */
    public final PaymentMethod paymentMethod;

    /**
     * Instantiates a new Set payment method command.
     *
     * @param id            the id
     * @param command       the command
     * @param currency      the currency
     * @param paymentMethod the payment method
     */
    public setPaymentMethodCommand(String id, String command, String currency, PaymentMethod paymentMethod) {
        super(id, command);
        this.currency = currency;
        this.paymentMethod = paymentMethod;
    }

}

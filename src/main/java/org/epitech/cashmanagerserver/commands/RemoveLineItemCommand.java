package org.epitech.cashmanagerserver.commands;

/**
 * The type Remove line item command.
 */
public class RemoveLineItemCommand extends BaseCommand<String> {

    /**
     * The Product id.
     */
    public final String productId;

    /**
     * The Name.
     */
    public final String name;

    /**
     * The Price.
     */
    public final Number price;

    /**
     * The Currency.
     */
    public final String currency;

    /**
     * The Quantity.
     */
    public final Number quantity;

    /**
     * Instantiates a new Remove line item command.
     *
     * @param id        the id
     * @param command   the command
     * @param productId the product id
     * @param name      the name
     * @param price     the price
     * @param currency  the currency
     * @param quantity  the quantity
     */
    public RemoveLineItemCommand(String id, String command, String productId,
                                 String name, Number price, String currency, Number quantity) {
        super(id, command);
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.currency = currency;
        this.quantity = quantity;
    }

}

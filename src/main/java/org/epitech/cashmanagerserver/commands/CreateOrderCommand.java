package org.epitech.cashmanagerserver.commands;

import org.epitech.cashmanagerserver.aggregates.Status;

/**
 * The type Create order command.
 */
public class CreateOrderCommand extends BaseCommand<String> {

    /**
     * The Status.
     */
    public final Status status;

    /**
     * The User name.
     */
    public final String userName;

    /**
     * Instantiates a new Create order command.
     *
     * @param id       the id
     * @param command  the command
     * @param status   the status
     * @param userName the user name
     */
    public CreateOrderCommand(String id, String command, Status status, String userName) {
        super(id, command);
        this.status = status;
        this.userName = userName;
    }

}

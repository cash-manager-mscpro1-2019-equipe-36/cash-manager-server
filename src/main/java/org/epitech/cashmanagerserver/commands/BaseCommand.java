package org.epitech.cashmanagerserver.commands;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

/**
 * The type Base command.
 *
 * @param <T> the type parameter
 */
public class BaseCommand<T> {

    /**
     * The Id.
     */
    @TargetAggregateIdentifier
    public final T id;

    /**
     * The Command.
     */
    public final T command;

    /**
     * Instantiates a new Base command.
     *
     * @param id      the id
     * @param command the command
     */
    public BaseCommand(T id, T command) {
        this.id = id;
        this.command = command;
    }

}

package org.epitech.cashmanagerserver.dto.commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderLineItemDTOTests {

    private OrderLineItemDTO orderLineItemDTO;

    @BeforeEach
    void setUp() {
        orderLineItemDTO = new OrderLineItemDTO();
    }

    @Test
    void testSetAndGetId() {
        orderLineItemDTO.setId("myId");
        Assertions.assertEquals("myId", orderLineItemDTO.getId());
    }

    @Test
    void testSetAndGetName() {
        orderLineItemDTO.setName("myName");
        Assertions.assertEquals("myName", orderLineItemDTO.getName());
    }

    @Test
    void testSetAndGetPrice() {
        orderLineItemDTO.setPrice(50);
        Assertions.assertEquals(50, orderLineItemDTO.getPrice());
    }

    @Test
    void testSetAndGetCurrency() {
        orderLineItemDTO.setCurrency("USD");
        Assertions.assertEquals("USD", orderLineItemDTO.getCurrency());
    }

    @Test
    void testSetAndGetQuantity() {
        orderLineItemDTO.setQuantity(1);
        Assertions.assertEquals(1, orderLineItemDTO.getQuantity());
    }

}

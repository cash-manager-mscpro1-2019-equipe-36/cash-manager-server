package org.epitech.cashmanagerserver.dto.commands;

import org.epitech.cashmanagerserver.aggregates.PaymentMethod;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PaymentMethodDTOTests {

    private PaymentMethodDTO paymentMethodDTO = null;

    @BeforeEach
    void setUp() {
        paymentMethodDTO = new PaymentMethodDTO();
    }

    @Test
    void testSetAndGetCurrency() {
        paymentMethodDTO.setCurrency("USD");
        Assertions.assertEquals("USD", paymentMethodDTO.getCurrency());
    }

    @Test
    void testSetAndGetUserPaymentMethod() {
        paymentMethodDTO.setPaymentMethod(PaymentMethod.CREDIT_CARD);
        Assertions.assertEquals(PaymentMethod.CREDIT_CARD, paymentMethodDTO.getPaymentMethod());
    }

}

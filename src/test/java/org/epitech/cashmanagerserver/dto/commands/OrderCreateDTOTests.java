package org.epitech.cashmanagerserver.dto.commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderCreateDTOTests {

    private OrderCreateDTO orderCreate = null;

    @BeforeEach
    void setUp() {
        this.orderCreate = new OrderCreateDTO();
    }

    @Test
    void testSetAndGetUserName() {
        this.orderCreate.setUserName("Kevin");
        Assertions.assertEquals("Kevin", this.orderCreate.getUserName());
    }

}

package org.epitech.cashmanagerserver.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderLineItemModelTests {

    private OrderLineItemModel orderLineItemModel;

    @BeforeEach
    void setUp() {
        String id = "501";
        String name = "T-Shirt";
        Number price = 50;
        String currency = "USD";
        Number quantity = 2;

        orderLineItemModel = new OrderLineItemModel(id, name, price, currency, quantity);
    }

    @Test
    void testOderLineItemId() {
        Assertions.assertEquals("501", orderLineItemModel.id);
    }

    @Test
    void testOderLineItemName() {
        Assertions.assertEquals("T-Shirt", orderLineItemModel.name);
    }

    @Test
    void testOderLineItemPrice() {
        Assertions.assertEquals(50, orderLineItemModel.price);
    }

    @Test
    void testOderLineItemCurrency() {
        Assertions.assertEquals("USD", orderLineItemModel.currency);
    }

    @Test
    void testOderLineItemQuantity() {
        Assertions.assertEquals(2, orderLineItemModel.quantity);
    }

}
